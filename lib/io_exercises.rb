# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.

def guessing_game
  random_number=rand(1..100)
  found = false
  user_guesses = 0
  while found == false
    puts "Try to Guess a number!"
    input = gets.chomp.to_i
    if input> random_number
      puts "Your guess #{input} is too high!"
    elsif input < random_number
      puts "Your guess #{input} is too low!"
    else
      found = true
    end
    user_guesses+=1
  end
  puts "Congratz!!! The number was #{random_number} and took #{user_guesses} guesses!"
end

if __FILE__ == $PROGRAM_NAME
  puts "Please give a filename"
  file_name = gets.chomp
  shuffled = File.new(file_name).each do |line|
    line.chars.shuffle.join
  end
  shuffled.close
end
